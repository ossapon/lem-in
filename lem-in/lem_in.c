/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <osapon@student.unit.ua>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/09 19:38:32 by osapon            #+#    #+#             */
/*   Updated: 2018/07/11 22:33:39 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

t_path		*create(char *name)
{
	t_path	*path;

	path = (t_path*)ft_memalloc(sizeof(t_path));
	path->room = ft_strdup(name);
	path->next = NULL;
	return (path);
}

t_path		*push_front(t_path *path, char *name)
{
	t_path	*tmp;

	tmp = create(name);
	tmp->next = path;
	return (tmp);
}

char		*find_name(t_rooms *rooms, int id)
{
	int		i;

	i = 0;
	while (rooms)
	{
		if (i == id)
			return (rooms->name);
		i++;
		rooms = rooms->prev;
	}
	return (NULL);
}

char		*room_name(t_path *path, int id)
{
	int		i;
	t_path	*tmp;

	i = 0;
	tmp = path;
	while (i < id)
	{
		i++;
		tmp = tmp->next;
	}
	return (tmp ? tmp->room : NULL);
}

int			main(void)
{
	t_lemin	*lemin;
	char	*line;

	lemin = (t_lemin*)ft_memalloc(sizeof(t_lemin));
	lemin->len = -1;
	get_data(&lemin, &line);
	check_if_there_start_end(lemin);
	make_links(&line, &lemin);
	if (!find_path(&lemin))
		exit(0);
	return (0);
}
