/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   additional_get_data_func.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <osapon@student.unit.ua>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/07 18:50:29 by osapon            #+#    #+#             */
/*   Updated: 2018/07/11 20:58:08 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	connect_rooms(char **line, t_lemin *t, t_rooms **l)
{
	t_rooms	*tm;

	if (!(tm = *l))
	{
		*l = t->room;
		(*l)->next = NULL;
		(*l)->prev = NULL;
		ft_strdel(line);
		return ;
	}
	else
		while (tm != NULL)
		{
			if (ft_strequ(tm->name, t->room->name)
	|| (tm->coord->x == t->room->coord->x && tm->coord->y == t->room->coord->y))
			{
				ft_printf("You can't have the same rooms or coordinates\n");
				exit(0);
			}
			tm = tm->prev;
		}
	(*l)->next = t->room;
	t->room->prev = *l;
	*l = (*l)->next;
	ft_strdel(line);
}

void	check_start(t_rooms *lemin)
{
	t_rooms	*tmp;

	tmp = lemin;
	while (tmp != NULL)
	{
		if (tmp->start != OK)
		{
			ft_printf("Looks like you have start already\n");
			exit(0);
		}
		tmp = tmp->prev;
	}
}

void	check_end(t_rooms *lemin)
{
	t_rooms	*tmp;

	tmp = lemin;
	while (tmp != NULL)
	{
		if (tmp->end != OK)
		{
			ft_printf("Looks like you have end already\n");
			exit(0);
		}
		tmp = tmp->prev;
	}
}

int		its_not_link(char *line)
{
	int	i;
	int	j;
	int	k;

	i = 0;
	j = 0;
	k = 0;
	while (line[k])
	{
		if (line[k] == '-')
			i++;
		else if (line[k] == ' ')
			j++;
		k++;
	}
	if (i >= 1 && j == 0)
		return (ERROR);
	return (OK);
}
