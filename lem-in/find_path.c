/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   find_path.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <osapon@student.unit.ua>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/08 16:46:45 by osapon            #+#    #+#             */
/*   Updated: 2018/07/11 22:34:34 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int			room_index(t_lemin **lemin, int manager)
{
	int		i;
	t_rooms	*tmp;

	i = 0;
	tmp = (*lemin)->room;
	while (tmp != NULL)
	{
		if (manager == START_MANAGER)
			if (tmp->start == 1)
				return (i);
		if (manager == END_MANAGER)
			if (tmp->end == 1)
				return (i);
		i++;
		tmp = tmp->prev;
	}
	return (-1);
}

void		path(t_array *tmp, t_lemin **lemin)
{
	int		i;
	int		j;
	t_path	*path;

	i = room_index(lemin, END_MANAGER);
	j = room_index(lemin, START_MANAGER);
	path = create(find_name((*lemin)->room, i));
	while (i != j)
	{
		i = tmp[i].p;
		path = push_front(path, find_name((*lemin)->room, i));
	}
	path->ant = (*lemin)->ants;
	(*lemin)->path = path;
}

void		get_next_room(t_lemin **lemin, int j, t_array *tmp, t_queue *queue)
{
	int		i;

	i = -1;
	while (++i < (*lemin)->len)
	{
		if ((*lemin)->matrix[j][i] != 0 && (tmp[i].v > tmp[j].v + 1))
		{
			queue->buff[queue->top++] = i;
			tmp[i].p = j;
			tmp[i].v = tmp[j].v + 1;
		}
	}
}

void		algorithm(t_lemin **lemin, t_array *tmp)
{
	int		i;
	int		j;
	int		k;
	t_queue	queue;

	j = 0;
	i = -1;
	ft_bzero(&queue, sizeof(t_queue));
	while (++i < (*lemin)->len)
	{
		tmp[i].p = 2147483647;
		tmp[i].v = 2147483647;
	}
	queue.buff[queue.top++] = room_index(lemin, START_MANAGER);
	while (queue.top != 0)
	{
		j = queue.buff[0];
		k = -1;
		while (++k < queue.top && k + 1 < 2048)
			queue.buff[k] = queue.buff[k + 1];
		queue.top--;
		if (j == room_index(lemin, END_MANAGER))
			break ;
		get_next_room(lemin, j, tmp, &queue);
	}
}

int			find_path(t_lemin **lemin)
{
	int		i;
	t_array	tmp[(*lemin)->len];

	tmp[room_index(lemin, START_MANAGER)].v = 0;
	algorithm(lemin, tmp);
	if (tmp[room_index(lemin, END_MANAGER)].p == 2147483647)
	{
		ft_printf("Looks like start doesn't connected to the end:(");
		return (0);
	}
	path(tmp, lemin);
	i = pathlen((*lemin)->path);
	carry_out_ants((*lemin)->path, i);
	return (1);
}
