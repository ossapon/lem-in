/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_data.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <osapon@student.unit.ua>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/07 18:44:47 by osapon            #+#    #+#             */
/*   Updated: 2018/07/11 22:38:27 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static int	save_start(char **line, t_lemin *tmp, t_lemin **lemin)
{
	int		i;

	i = 0;
	ft_strdel(line);
	get_next_line(0, line);
	ft_printf("%s\n", *line);
	check_start((*lemin)->room);
	error_manager(line, lemin, START_MANAGER);
	tmp->room->start = 1;
	while ((*line)[i] != ' ')
		i++;
	tmp->room->name = (char*)ft_memalloc((size_t)i);
	ft_strncpy(tmp->room->name, *line, (size_t)i);
	ft_bzero(&((tmp->room->name)[i]), ft_strlen(tmp->room->name) - i);
	tmp->room->coord->y = ft_atoi((*line) + i);
	i += (int)ft_digit_lenght((size_t)tmp->room->coord->y, 10);
	tmp->room->coord->x = ft_atoi((*line) + i + 2);
	tmp->room->start = 1;
	connect_rooms(line, tmp, &(*lemin)->room);
	return (ERROR);
}

static int	save_end(char **line, t_lemin *tmp, t_lemin **lemin)
{
	int		i;

	i = 0;
	ft_strdel(line);
	get_next_line(0, line);
	ft_printf("%s\n", *line);
	check_end((*lemin)->room);
	error_manager(line, lemin, END_MANAGER);
	tmp->room->end = 1;
	while ((*line)[i] != ' ')
		i++;
	tmp->room->name = (char*)ft_memalloc((size_t)i);
	ft_strncpy(tmp->room->name, *line, (size_t)i);
	ft_bzero(&((tmp->room->name)[i]), ft_strlen(tmp->room->name) - i);
	tmp->room->coord->y = ft_atoi((*line) + i);
	i += (int)ft_digit_lenght((size_t)tmp->room->coord->y, 10);
	tmp->room->coord->x = ft_atoi((*line) + i + 2);
	tmp->room->end = 1;
	connect_rooms(line, tmp, &(*lemin)->room);
	return (1);
}

static int	get_additional_rooms(char **line, t_lemin *tmp, t_lemin **lemin)
{
	int		i;

	i = 0;
	error_manager(line, lemin, ROOM_MANAGER);
	while ((*line)[i] != ' ')
		i++;
	tmp->room->name = (char*)ft_memalloc((size_t)i);
	ft_strncpy(tmp->room->name, *line, (size_t)i);
	ft_bzero(&((tmp->room->name)[i]), ft_strlen(tmp->room->name) - i);
	tmp->room->coord->y = ft_atoi((*line) + i);
	i += (int)ft_digit_lenght((size_t)tmp->room->coord->y, 10);
	tmp->room->coord->x = ft_atoi((*line) + i + 2);
	connect_rooms(line, tmp, &(*lemin)->room);
	return (1);
}

static int	get_rooms(t_lemin **lemin, char **line)
{
	t_lemin *tmp;

	tmp = (t_lemin *)ft_memalloc(sizeof(t_lemin));
	tmp->room = (t_rooms *)ft_memalloc(sizeof(t_rooms));
	tmp->room->coord = (t_coord *)ft_memalloc(sizeof(t_coord));
	(*lemin)->len++;
	if (ft_strequ("##start", *line) && save_start(line, tmp, lemin))
		return (OK);
	else if (ft_strequ("##end", *line) && save_end(line, tmp, lemin))
		return (OK);
	else if (its_not_link(*line) == OK
			&& get_additional_rooms(line, tmp, lemin))
		return (OK);
	free(tmp->path);
	while (tmp->room)
	{
		free(tmp->room->coord);
		free(tmp->room->name);
		free(tmp->room);
		tmp->room = tmp->room->prev;
	}
	free(tmp);
	return (ERROR);
}

void		get_data(t_lemin **lemin, char **l)
{
	int		y;

	y = -1;
	while (get_next_line(0, l) != OK)
	{
		ft_printf("%s\n", *l);
		if ((*l)[0] == '#' &&
			!ft_strequ("##start", *l) &&
			!ft_strequ("##end", *l))
			ft_strdel(l);
		else if ((*lemin)->ants == 0)
			error_manager(l, lemin, ANT_MANAGER);
		else if (get_rooms(lemin, l) != OK)
			break ;
	}
	((*lemin)->matrix) = (int**)malloc(sizeof(int*) * (*lemin)->len);
	while (++y < (*lemin)->len)
		((*lemin)->matrix)[y] = (int*)ft_memalloc(sizeof(int) * (*lemin)->len);
}
