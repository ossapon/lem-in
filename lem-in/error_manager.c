/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error_manager.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <osapon@student.unit.ua>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/07 18:57:48 by osapon            #+#    #+#             */
/*   Updated: 2018/07/10 19:44:31 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int			my_room_name(char **tmp)
{
	int		i;

	i = 0;
	while ((*tmp)[i])
	{
		if ((*tmp)[0] == 'L' || (*tmp)[i] == '-')
		{
			ft_printf("Invalid room name :(\n");
			ft_strdel(tmp);
			exit(0);
		}
		i++;
	}
	return (OK);
}

static int	check_spaces(char **line)
{
	int		i;
	int		j;

	i = 0;
	j = 0;
	while ((*line)[i])
	{
		if ((*line)[i] == ' ')
			j++;
		i++;
	}
	if (j != 2)
	{
		ft_printf("Invalid room name - check space's :(\n");
		return (ERROR);
	}
	return (OK);
}

static void	ants_check(char **line, t_lemin **lemin)
{
	int		i;

	i = 0;
	if ((*lemin)->flag == 0)
	{
		while ((*line)[i++])
			if (!ft_isdigit((*line)[i - 1]))
			{
				ft_strdel(line);
				ft_printf("Ants can be only number, no other symbols :(\n");
				exit(0);
			}
	}
	else if ((*lemin)->ants > 2147483647 || (*lemin)->ants < 1)
	{
		ft_printf("Ants can be only number from 1 to 2147483647 :(\n");
		exit(0);
	}
}

static int	check_digits(char **line)
{
	int		i;
	int		j;

	i = 0;
	j = 0;
	while ((*line)[i] != ' ')
		i++;
	i++;
	while ((*line)[i])
	{
		if (!ft_isdigit((*line)[i]) && (*line)[i] != ' ')
		{
			ft_printf("coordinates can be only positive numbers :(\n");
			return (ERROR);
		}
		if ((*line)[i++] == ' ' && (*line)[i] != '\0')
			j++;
	}
	if (j != 1)
	{
		ft_printf("You forgot one number :(\n");
		return (ERROR);
	}
	return (OK);
}

int			error_manager(char **line, t_lemin **lemin, int manager)
{
	if (manager == ANT_MANAGER)
	{
		ants_check(line, lemin);
		(*lemin)->ants = ft_atoi(*line);
		(*lemin)->flag = 1;
		ft_strdel(line);
		ants_check(line, lemin);
	}
	else
	{
		if (!my_room_name(line))
		{
			if (check_spaces(line) == ERROR || check_digits(line) == ERROR)
			{
				ft_strdel(line);
				exit(0);
			}
		}
	}
	return (ERROR);
}
