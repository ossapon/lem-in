/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <osapon@student.unit.ua>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/09 19:37:40 by osapon            #+#    #+#             */
/*   Updated: 2018/07/08 17:11:30 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEM_IN_H
# define LEM_IN_H
# define ERROR 1
# define OK 0
# define ANT_MANAGER 1
# define ROOM_MANAGER 2
# define START_MANAGER 3
# define END_MANAGER 4

# include "libft/ft_printf.h"
# include "libft/get_next_line.h"

typedef struct		s_coord
{
	int				x;
	int				y;
}					t_coord;

typedef struct		s_rooms
{
	char			*name;
	int				start;
	int				end;
	t_coord			*coord;
	struct s_rooms	*next;
	struct s_rooms	*prev;

}					t_rooms;

typedef struct		s_path
{
	int				ant;
	char			*room;
	struct s_path	*next;
}					t_path;

typedef struct		s_lemin
{
	int				flag;
	int				len;
	int				**matrix;
	int				ants;
	t_rooms			*room;
	t_path			*path;
}					t_lemin;

typedef struct		s_array
{
	int				v;
	int				p;
}					t_array;

typedef struct		s_queue
{
	int				buff[50000];
	int				top;
}					t_queue;

void				get_data(t_lemin **lemin, char **l);
int					its_not_link(char *line);
int					error_manager(char **line, t_lemin **lemin, int manager);
void				connect_rooms(char **line, t_lemin *tmp, t_rooms **lemin);
void				check_start(t_rooms *lemin);
void				check_end(t_rooms *lemin);
void				check_if_there_start_end(t_lemin *lemin);
void				make_links(char **l, t_lemin **lemin);
int					find_path(t_lemin **lemin);
int					pathlen(t_path *path);
void				carry_out_ants(t_path *path, int pathlen);
t_path				*push_front(t_path *path, char *name);
char				*find_name(t_rooms *rooms, int id);
char				*room_name(t_path *path, int id);
t_path				*create(char *name);

#endif
