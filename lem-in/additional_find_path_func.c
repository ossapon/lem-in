/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   additional_find_path_func.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <osapon@student.unit.ua>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/08 16:51:12 by osapon            #+#    #+#             */
/*   Updated: 2018/07/08 16:55:49 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void		print_path(int *arr, t_path *path, int len)
{
	while (len--)
		if (arr[len] && len != 0)
			ft_printf("L%d-%s ", arr[len], room_name(path, len));
}

int			array_diff(int *arr, int len, int j)
{
	int		i;

	i = -1;
	while (++i < len)
		if (arr[i] != j)
			return (1);
	return (0);
}

void		array_shift(int *arr, int len)
{
	int		tmp;

	arr[len] = 0;
	while (--len > 0)
	{
		tmp = arr[len];
		arr[len] = arr[len - 1];
		arr[len - 1] = tmp;
	}
}

int			pathlen(t_path *path)
{
	int		i;
	t_path	*tmp;

	i = 0;
	tmp = path;
	while (tmp)
	{
		i++;
		tmp = tmp->next;
	}
	return (i);
}

void		carry_out_ants(t_path *path, int pathlen)
{
	int		arr[pathlen];
	int		ant;
	int		len;
	int		i;

	ft_bzero(arr, sizeof(arr));
	ant = 0;
	len = pathlen;
	i = 0;
	while (array_diff(arr, len, 0) || i == 0)
	{
		arr[0] = ant < path->ant ? ++ant : 0;
		print_path(arr, path, len);
		array_shift(arr, len);
		if (array_diff(arr, len, 0))
			ft_printf("\n");
		i++;
	}
}
