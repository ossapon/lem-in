/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   make_links.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osapon <osapon@student.unit.ua>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/07 19:04:50 by osapon            #+#    #+#             */
/*   Updated: 2018/07/11 22:09:57 by osapon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void		check_if_there_start_end(t_lemin *lemin)
{
	int		i;
	t_rooms	*rooms;

	i = 0;
	rooms = lemin->room;
	while (rooms != NULL)
	{
		if (rooms->start != OK || rooms->end != OK)
			i++;
		rooms = rooms->prev;
	}
	if (i != 2)
	{
		ft_printf("You need at least one start and one end\n");
		exit(0);
	}
}

static int	check_link(char **line)
{
	int		i;
	int		j;

	i = -1;
	j = 0;
	while ((*line)[++i])
	{
		if ((*line)[i] == '-')
			j++;
		if ((*line)[i] == ' ')
			return (ERROR);
	}
	if (j != 1)
	{
		ft_printf("CAUTION! INVALID LINKS FORMAT\n");
		ft_strdel(line);
		return (ERROR);
	}
	return (OK);
}

static int	check_room_name(char *name, t_lemin **lemin)
{
	int		i;
	t_rooms *tmp;

	tmp = (*lemin)->room;
	i = 0;
	while (tmp)
	{
		if (ft_strequ(tmp->name, name))
			return (i);
		i++;
		tmp = tmp->prev;
	}
	return (-1);
}

static void	connect_links(char **line, t_lemin **lemin)
{
	int		y;
	int		x;
	int		i;
	char	**names;

	names = ft_strsplit((*line), '-');
	if (ft_strequ(names[0], names[1]) != 1)
	{
		if ((y = check_room_name(names[0], lemin)) != -1
			&& (x = check_room_name(names[1], lemin)) != -1)
		{
			(*lemin)->matrix[y][x] = 1;
			(*lemin)->matrix[x][y] = 1;
		}
		else
			ft_printf("LOOOOOL!!! You can't connect that doesn't exists\n");
	}
	else
		ft_printf("The room can't be connected to itself\n");
	i = -1;
	while (names[++i])
		free(names[i]);
	free(names);
	ft_strdel(line);
}

void		make_links(char **l, t_lemin **lemin)
{
	if (check_link(l) == OK)
		connect_links(l, lemin);
	if (*l)
		ft_strdel(l);
	while (get_next_line(0, l) != OK)
	{
		if ((*l)[0] == '\0')
			break ;
		else
		{
			ft_printf("%s\n", *l);
			if ((*l)[0] == '#')
				ft_strdel(l);
			else if (check_link(l) == OK)
				connect_links(l, lemin);
		}
	}
}
