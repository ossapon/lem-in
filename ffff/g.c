/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   g.c                                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgres <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/06 16:29:24 by bgres             #+#    #+#             */
/*   Updated: 2018/03/06 16:29:26 by bgres            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	push_ants_v2(void)
{
	int		i;
	t_rooms	**ant_mas;

	ant_mas = (t_rooms **)malloc(sizeof(t_rooms *) * g_ants);
	i = 0;
	while (i < g_ants)
		ant_mas[i++] = NULL;
	i = 0;
	while (g_end->ant_n < g_ants)
	{
		if (i < g_ants)
			turn(i, &ant_mas[i]);
		i++;
		if (i == g_ants)
		{
			i = 0;
			ft_printf("\n");
		}
	}
	if (i != 0)
		ft_printf("\n");
	free(ant_mas);
}

int		main(void)
{
	t_rooms *last;
	char	*line;
	int		k;

	k = 0;
	line = NULL;
	g_ht = NULL;
	g_i = 0;
	g_j = 0;
	g_end = NULL;
	g_start = NULL;
	g_ants = get_ant();
	last = NULL;
	while (get_room(&last, &line) == 1)
		;
	if (!g_start || !g_end)
		strt_end_error(line);
	while (get_links(&line, &last, k) == 1)
		k = 1;
	ft_algoritm(&line);
	return (0);
}
