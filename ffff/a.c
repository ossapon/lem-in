/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   a.c                                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgres <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/06 16:28:46 by bgres             #+#    #+#             */
/*   Updated: 2018/03/06 16:28:49 by bgres            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int		get_ant(void)
{
	char	*line;
	long	ants;
	int		j;
	char	*a;

	ants = 0;
	j = 0;
	g_nl_ret = get_next_line(0, &line);
	ft_printf("%s\n", line);
	while (line[0] == '#' && !ft_strequ(line, "##start")
		&& !ft_strequ(line, "##end"))
	{
		free(line);
		g_nl_ret = get_next_line(0, &line);
		ft_printf("%s\n", line);
	}
	a = line;
	while (ft_isdigit(*(line + j)))
		ants = ants * 10 + *(line + j++) - 48;
	if (j < 1 || *(line + j) != '\0' || (ants > 0x7FFFFFFF || ants < 1))
		ant_error(a);
	free(a);
	return ((int)ants);
}

void	ant_error(char *line)
{
	ft_printf("Oops, something wrong with ants\n");
	free(line);
	exit(0);
}

void	room_error(t_rooms *tmp, t_rooms **last, char *error_txt)
{
	t_rooms		*gg;

	ft_printf("%s\n", error_txt);
	if (tmp)
	{
		free(tmp->name);
		free(tmp);
	}
	while ((*last))
	{
		gg = *last;
		free((*last)->name);
		*last = (*last)->prvs;
		free(gg);
	}
	system("leaks a.out");
	exit(0);
}

void	strt_end_error(char *line)
{
	ft_printf("Oops, you need just one start and one end\n");
	free(line);
	system("leaks a.out");
	exit(0);
}

int		count_a(char *line, char a)
{
	int		i;

	i = 0;
	while (*line != '\0')
	{
		if (*line == a)
			i++;
		line++;
	}
	return (i);
}
