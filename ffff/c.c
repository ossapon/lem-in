/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   c.c                                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgres <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/06 16:28:58 by bgres             #+#    #+#             */
/*   Updated: 2018/03/06 16:29:00 by bgres            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	skip_dig(char **line, int *j)
{
	if (**line == '-')
		(*line)++;
	if (**line == ' ')
	{
		*j = 0;
		return ;
	}
	while (ft_isdigit(*(*line)++))
		j++;
}

int		check_space(char *line, t_rooms *tmp, t_rooms **last)
{
	if (line[0] == ' ' || count_a(line, ' ') != 0)
		room_error(tmp, last, "Oops, your room too bad");
	return (0);
}

int		add_room(int fgl, char *line, t_rooms **last)
{
	int		j;
	t_rooms	*tmp;
	char	*a;

	a = check_one(&line, &tmp, fgl, &j);
	if ((fgl == 1 && g_start != NULL) || (fgl == 2 && g_end != NULL))
		strt_end_error(line);
	if (count_a(line, ' ') != 2 || line[0] == ' ')
		return (check_space(line, tmp, last));
	while (*(line++) != ' ')
		j++;
	tmp = room_assign(fgl, j, line, last);
	if (*(line) == ' ' && (j = 0) == 0)
		room_error(tmp, last, "Oops, your room too bad");
	tmp->y = ft_atoi(line);
	skip_dig(&line, &j);
	if ((j < 1 || *(-1 + line) != ' ') && (j = 0) == 0)
		room_error(tmp, last, "Oops, your room feels bad about coordinate Y");
	tmp->x = ft_atoi(line);
	skip_dig(&line, &j);
	if (j < 1 || *(-1 + line) != '\0')
		room_error(tmp, last, "Oops, your room feels bad about coordinate X");
	return (a_r_srt_end(fgl, tmp, a, last));
}

int		get_room(t_rooms **last, char **line)
{
	int		i;

	g_nl_ret = get_next_line(0, line);
	ft_printf("%s\n", *line);
	while ((*line)[0] == '#' && !ft_strequ(*line, "##start")
		&& !ft_strequ(*line, "##end"))
	{
		free(*line);
		g_nl_ret = get_next_line(0, line);
		ft_printf("%s\n", *line);
	}
	if (ft_strequ(*line, "##start"))
		i = add_room(1, *line, last);
	else if (ft_strequ(*line, "##end"))
		i = add_room(2, *line, last);
	else
		i = add_room(0, *line, last);
	if (i == 1)
		return (1);
	return (0);
}

t_rooms	*chek_name(t_rooms *tail, char *tmp)
{
	while (tail)
	{
		if (ft_strequ(tail->name, tmp))
		{
			return (tail);
		}
		tail = tail->prvs;
	}
	return (NULL);
}
