/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f.c                                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgres <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/06 16:29:19 by bgres             #+#    #+#             */
/*   Updated: 2018/03/06 16:29:21 by bgres            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	error_for_alg(char **line)
{
	if (g_i > 1 && g_j > 1 && ft_strlen(*line) == 0)
		return ;
	ft_printf("%s\n", *line);
	if (g_i < 1)
		ft_printf("Oops, you forgot rooms\n");
	if (g_j < 1)
		ft_printf("Oops, you forgot links\n");
	if (ft_strlen(*line) > 0)
		ft_printf("wrong word\n");
	exit(0);
}

void	ft_algoritm(char **line)
{
	t_links	*tmp;
	t_rooms	*tmp2;
	int		j;

	if (g_i < 1 || g_j < 1 || g_nl_ret != 0)
		error_for_alg(line);
	j = 0;
	tmp2 = g_end;
	tmp = g_end->links;
	tmp2->len_to_end = 0;
	get_way(g_end, 0, &j);
	if (j < 1)
	{
		ft_printf("Oops, you need at least one way\n");
		system("leaks a.out");
		exit(0);
	}
	g_start->blocl = 1;
	push_ants_v2();
}

void	turn_assign(t_rooms **tmp, t_rooms **tmp2,
					t_links **tmpl, t_rooms **ant)
{
	if (*ant == NULL)
	{
		*ant = g_start;
	}
	*tmp = *ant;
	*tmpl = (*ant)->links;
	*tmp2 = (*ant)->links->room;
}

void	turn_out(t_rooms **tmp2, t_rooms **ant, int i)
{
	if (*ant != NULL)
		(*ant)->blocl = 0;
	(*ant) = *tmp2;
	if ((*ant)->strt_end == 2)
	{
		ft_printf("L%d-%s ", i + 1, (*ant)->name);
		g_end->ant_n = g_end->ant_n + 1;
		(*ant)->blocl = 0;
	}
	else
	{
		ft_printf("L%d-%s ", i + 1, (*ant)->name);
		(*tmp2)->blocl = 1;
	}
}

void	turn(int i, t_rooms **ant)
{
	t_rooms		*tmp;
	t_rooms		*tmp2;
	t_links		*tmpl;
	int			g;

	g = 0;
	turn_assign(&tmp, &tmp2, &tmpl, ant);
	while (tmpl != NULL && tmp->strt_end != 2)
	{
		if (((tmp2->len_to_end >= tmpl->room->len_to_end
			&& tmpl->room->len_to_end != -1) && tmpl->room->blocl == 0))
		{
			g = 1;
			tmp2 = tmpl->room;
		}
		if ((g_ants - i - 1 > tmpl->room->len_to_end
			&& tmpl->room->len_to_end != -1) && tmpl->room->blocl == 0
			&& tmpl->room->len_to_end < (*ant)->len_to_end && g == 0)
			tmp2 = tmpl->room;
		tmpl = tmpl->next;
	}
	if (tmp2 && tmp2->blocl == 0 && tmp->strt_end != 2)
		turn_out(&tmp2, ant, i);
}
