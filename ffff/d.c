/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   d.c                                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgres <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/06 16:29:03 by bgres             #+#    #+#             */
/*   Updated: 2018/03/06 16:29:05 by bgres            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	add_link(t_rooms *a, t_rooms *b, int flg)
{
	t_links	*tmp;

	g_j++;
	if (!a->links)
	{
		a->links = (t_links *)malloc(sizeof(t_links));
		a->links->room = b;
		a->links->next = NULL;
		if (flg == 0)
			add_link(b, a, 1);
	}
	else
	{
		tmp = (t_links *)malloc(sizeof(t_links));
		tmp->room = b;
		tmp->next = a->links;
		a->links = tmp;
		if (flg == 0)
			add_link(b, a, 1);
	}
}

void	make_ht(t_rooms *last)
{
	size_t	i;
	t_ht	*tmp;

	i = 0;
	if (g_ht == NULL)
	{
		g_ht = (t_ht **)ft_memalloc(sizeof(t_ht *) * g_i);
		while (i < g_i)
			g_ht[i++] = (t_ht *)ft_memalloc(sizeof(t_ht));
		while (last)
		{
			i = hash_f(last->name);
			if (g_ht[i])
			{
				tmp = (t_ht *)malloc(sizeof(t_ht));
				tmp->next = g_ht[i];
				tmp->room = last;
				g_ht[i] = tmp;
			}
			else
				g_ht[i]->room = last;
			last = last->prvs;
		}
	}
}

size_t	hash_f(char *s)
{
	size_t	hashval;
	size_t	j;

	j = 0;
	hashval = 0;
	while (s[j] != '\0')
	{
		hashval = s[j] + 31 * hashval;
		j++;
	}
	return (hashval % g_i);
}

t_rooms	*get_from_ht(char *tmp, char **line)
{
	t_ht	*a;
	size_t	i;

	i = hash_f(tmp);
	a = g_ht[i];
	if (a == NULL || a->room == NULL || a->room->name == NULL)
		link_error(line, "Oops, your link does not exist");
	while (!ft_strequ(a->room->name, tmp))
	{
		a = a->next;
		if (a->room == NULL)
			link_error(line, "Oops, your link does not exist");
	}
	return (a->room);
}

void	skip_two(int *i, int *j, t_rooms **last, char **line)
{
	*j = 0;
	*i = 0;
	make_ht(*last);
	while ((*line)[0] == '#' && !ft_strequ(*line, "##start")
		&& !ft_strequ(*line, "##end"))
	{
		ft_printf("%s\n", *line);
		free(*line);
		g_nl_ret = get_next_line(0, line);
	}
	if (ft_strequ(*line, "##start") || ft_strequ(*line, "##end"))
		link_error(line, "Oops, start\\end must be higher");
}
