/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   e.c                                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgres <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/06 16:29:10 by bgres             #+#    #+#             */
/*   Updated: 2018/03/06 16:29:11 by bgres            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	f_one(char **line, int k)
{
	if (k > 0)
		ft_printf("%s\n", *line);
	free(*line);
	g_nl_ret = get_next_line(0, line);
}

int		check_link(char **line)
{
	if (count_a(*line, '-') > 1)
		link_error(line, "Ooops, please less \"-\"\n");
	return (0);
}

int		get_links(char **line, t_rooms **last, int k)
{
	char	*tmp;
	t_rooms *tail[2];
	int		i;
	int		j;

	skip_two(&i, &j, last, line);
	if (count_a(*line, '-') != 1 || (*line)[0] == '-')
		return (check_link(line));
	while ((*line)[i] != '-')
		i++;
	tmp = ft_strndup(*line, i);
	if ((tail[0] = get_from_ht(tmp, line)) == NULL)
		link_error(line, "Oops, your link does not exist");
	i++;
	free(tmp);
	while ((*line)[i + j] != '\0')
		j++;
	tmp = ft_strndup(&(*line)[i], j);
	if ((tail[1] = get_from_ht(tmp, line)) == NULL)
		link_error(line, "Oops, your link does not exist");
	free(tmp);
	add_link(tail[0], tail[1], 0);
	f_one(line, k);
	return (1);
}

void	link_error(char **line, char *txt)
{
	ft_printf("%s\n", *line);
	free(*line);
	ft_printf("%s\n", txt);
	system("leaks a.out");
	exit(0);
}

void	get_way(t_rooms *tmp, int i, int *j)
{
	t_links	*tmp1;
	t_rooms	*tmp2;

	if (tmp->strt_end == 1)
	{
		*j = *j + 1;
		tmp->len_to_end = i + 1;
		return ;
	}
	tmp2 = tmp;
	tmp1 = tmp->links;
	tmp2->len_to_end = i;
	if (tmp1)
		while (tmp1)
		{
			if (tmp1->room->len_to_end == -1 || tmp1->room->len_to_end > i + 1)
				get_way(tmp1->room, i + 1, j);
			tmp1 = tmp1->next;
		}
}
