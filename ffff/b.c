/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   b.c                                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bgres <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/06 16:28:53 by bgres             #+#    #+#             */
/*   Updated: 2018/03/06 16:28:54 by bgres            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	a_t_l(char *line, t_rooms **last, t_rooms *tmp)
{
	t_rooms *tmp2;

	g_i++;
	tmp2 = *last;
	if (!tmp2)
	{
		(*last) = tmp;
		(*last)->next = NULL;
		(*last)->prvs = NULL;
		(*last)->links = NULL;
		free(line);
		return ;
	}
	else
		while (tmp2 != NULL)
		{
			if (ft_strequ(tmp2->name, tmp->name)
				|| (tmp2->x == tmp->x && tmp2->y == tmp->y))
				room_error(tmp, last, "Oops, your room has a double\n");
			tmp2 = tmp2->prvs;
		}
	(*last)->next = tmp;
	tmp->prvs = *last;
	*last = (*last)->next;
	free(line);
}

t_rooms	*room_assign(int fgl, int j, char *line, t_rooms **last)
{
	t_rooms	*tmp;

	tmp = (t_rooms *)malloc(sizeof(t_rooms));
	tmp->links = NULL;
	tmp->len_to_end = -1;
	tmp->strt_end = fgl;
	tmp->blocl = 0;
	tmp->name = ft_strndup(line - j - 1, j);
	if (count_a(tmp->name, '-') != 0 || tmp->name[0] == 'L')
		room_error(tmp, last, "Oops, your room feels bad about it name");
	tmp->strt_end = tmp->strt_end | (unsigned char)fgl;
	return (tmp);
}

int		a_r_srt_end(int fgl, t_rooms *tmp, char *a, t_rooms **last)
{
	if (fgl == 2)
		g_end = tmp;
	if (fgl == 1)
		g_start = tmp;
	a_t_l(a, last, tmp);
	return (1);
}

void	skip(char **line)
{
	free(*line);
	g_nl_ret = get_next_line(0, line);
	while ((*line)[0] == '#' && !ft_strequ(*line, "##start")
		&& !ft_strequ(*line, "##end"))
	{
		ft_printf("%s\n", *line);
		free(*line);
		g_nl_ret = get_next_line(0, line);
	}
}

char	*check_one(char **line, t_rooms **tmp, int fgl, int *j)
{
	*tmp = NULL;
	*j = 0;
	if (fgl > 0)
	{
		skip(line);
		ft_printf("%s\n", *line);
	}
	return (*line);
}
